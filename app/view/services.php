<div id="content">
	<div class="row">
		<h1>Services</h1>
		<div class="inner-services">
			<dl>
				<dt id="service1">Water Line Replacements</dt>
				<dd>
					<h2>Plumbing Companies Near Me</h2>
					<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
					<p>Turn on your faucet and you should get clean, clear and flowing water. This water is delivered to you from a water line from the street, under your property and, finally, to your faucet. Generally, a water supply line works fine for a number of years, but if you notice water discoloration, low pressure, or an unusual damp area in your yard, you may have a water line leak. If this is the case, you can contact J & T Plumbing Licensed and Insured to repair any of your problems in Waxahachie, TX and the surrounding areas quickly before it gets any worse.</p>
					<p>If you are renovating your home, then you should contact a reliable plumber to inspect your existing water line to measure water pressure and flow to determine the right pipe configuration. It’s important to get it right before you start your project. We offer easy, low cost repairs and installation.</p>
					<p>Over time, your pipes will start to wear and rust. Leaks and breaks in the water line can occur, which will lead to flooding. We offer 24 hr plumbing services to repair or install a new water line should an emergency arise. We will check and clear all your pipes of build up and can determine whether water line replacements are needed. We also offer gas line replacements as needed.</p>
					<p>If you are in need of sewer line repairs, need a new water line installed, or are concerned about any other plumbing services in Waxahachie, TX and the surrounding areas, contact J & T Plumbing Licensed and Insured today to get all your plumbing questions answered by a trained plumber.</p>
				</dd>
			</dl>
			<dl id="service2">
				<dt>Sewer Line Repairs</dt>
				<dd>
					<h2>Emergency Plumber Near Me</h2>
					<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
					<p>There are many tasks involved in underground sewer repair, ranging from simple drain cleaning to complex excavations. No matter what the job is, our plumber will diagnose the problem and provide the appropriate solution. Sewer line repairs are dirty jobs. They’re one of the few tasks that even the staunchest, most do-it-yourself enthusiast would like to avoid. Don't get dirty, have a professional take on the job. We have the skills, tools, and knowledge to clean up the mess. Whether you are in need of toilet replacements or replacements for other fixtures, we are the company for you.</p>
					<p>We have a lot of experience with gas and water line replacements and know what to do in any given situation. Our expertise has carried us through many emergencies, even when things take an unexpected turn for the worst.</p>
					<p>You can trust the plumbing services offered by J & T Plumbing Licensed and Insured. We are the experts you need when it comes to sewer services. If you're looking for an emergency plumber in Waxahachie, TX and the surrounding areas, contact J & T Plumbing Licensed and Insured today!</p>
				</dd>
			</dl>
			<dl id="service3">
				<dt>Hydrostatic Testing</dt>
				<dd>
					<h2>Emergency Plumber Near Me</h2>
					<p class="phone"><?php $this->info(["phone","tel"]); ?></p>
					<p>A hydrostatic test is a way to test whether a pressure vessel, like a pipeline, is as strong as it should be. By filling the vessel with a liquid, and then shutting off the valve, we can tell if there is a pressure loss. This sort of service is important for homes and buildings.</p>
					<p>At J & T Plumbing Licensed and Insured, we offer a wide array of 24 hr plumbing services to make sure your fixtures are all working properly. Our local plumbers will come out and provide this test to check for any leaks in your plumbing before deciding on sewer line repairs. We will make any and all water line replacements necessary.</p>
					<p>For all of our hydrostatic testing services, contact us at J & T Plumbing Licensed and Insured. We promise to prove ourselves as one of the best plumbing companies in Waxahachie, TX and the surrounding areas!</p>
				</dd>
			</dl>
		</div>
	</div>
</div>
