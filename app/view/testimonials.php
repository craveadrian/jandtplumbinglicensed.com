<div id="content">
	<div class="row">
		<h1>TESTIMONIALS</h1>
		<div class="inner-reviews">
				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>My shower faucet was practically running non stop. Jimmy came over, diagnosed the problem and had me back in business in no time, AND he was very reasonable! I totally recommend you call him if you're looking for a plumber??!</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Great service! Quick response! Awesome rates! He had a great attitude and completed the job efficiently. Will definitely be calling again.</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Jimmy was awesome! He came to the house late when we had a late night emergency. He was nice and professional as well. We will definitely save his number for future use!</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Was very pleased with the quality of work provided. Jimmy was very professional and knowledgeable. I will definitely hire again in the future.</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Jimmy form J&T plumbing retrofitted a tankless water heater to my failing storage water heater. I waited this long to make this a proven review. I was taking bids, and there were a lot of high dollar companies who came a looked at and quoted on the job. Of about 6 people who were interested, Jimmy was the only RMP (Responsible Master Plumber) who cared to go up to the attic to check the size of the gas line. He got the city permits, and the inspector passed it without any hesitation. I would recommend J & T Plumbing.</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Jimmy came out within an hour of calling to clear out our main drain line. We had water backing up in to the house because of the blockage. Of course this occurred on a Saturday evening of a holiday weekend. Jimmy did a great job, quickly, and his rates were very reasonable (did I mention he came out on a Saturday evening on a holiday weekend?).</dd>
				</dl>

			 	<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Jimmy came out late at night and shut our water off for us and came out the next morning and replaced water heater, awesome company they also warranty their work</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Awesome company. Great customer service. Excellent work. I will definitely use them again.</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>Excellent plumber. Great personality. Easy to work with. Fix all our problems. Very good customer service.</dd>
				</dl>

				<dl>
					<dt>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</dt>
					<dd>This company is quick to respond to service and got job done in great time!! Really good at communication and reliability to get work done! Also warranty his work! Great working with you jimmy!!!</dd>
				</dl>
		</div>
	</div>
</div>
