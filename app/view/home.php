<div id="welcome">
	<div class="row">
		<div class="wlcLeft col-6 fl">
			<h2>WELCOME TO</h2>
			<h1>J & T Plumbing Licensed and Insured</h1>
			<p>As every property owner knows, having a well-maintained plumbing system is a vital aspect to any buildig. Not only does it need to be in working order for your tenants, but, without proper attention, damages can result, leading to very costly repairs, especially for a larger building.</p>
			<p>J & T Plumbing Licensed and Insured is the top pick for homes and businesses in Waxahachie, TX and the surrounding areas.</p>

			<a href="service#content" class="btn">read more</a>
			<div class="social">
				<a href="<?php $this->info("fb_link"); ?>" target="_blank"> <img src="public/images/sprite.png" alt="facebook icon" class="bg-fb"> </a>
				<a href="<?php $this->info("gp_link"); ?>" target="_blank"> <img src="public/images/sprite.png" alt="google icon" class="bg-gp"> </a>
				<a href="<?php $this->info("tt_link"); ?>" target="_blank"> <img src="public/images/sprite.png" alt="twitter icon" class="bg-tt"> </a>
				<a href="<?php $this->info("li_link"); ?>" target="_blank"> <img src="public/images/sprite.png" alt="linked icon" class="bg-li"> </a>
			</div>
		</div>
		<div class="wlcRight col-6 fl">
			<img src="public/images/content/img1.jpg" alt="Plumbing" class="img1">
			<img src="public/images/content/img2.png" alt="Tools" class="img2">
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="whychoosus">
	<div class="row">
		<h1>WHY CHOOSE US</h1>
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/wcu1.png" alt="Images 1"> </dt>
				<dd> Fast & Reliable Service </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/wcu2.png" alt="Images 2"> </dt>
				<dd> Free Estimates/Quotes </dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/wcu3.png" alt="Images 3"> </dt>
				<dd> 100% Customer Satisfaction </dd>
			</dl>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="container">
			<dl>
				<dt> <img src="public/images/content/svc1.jpg" alt="Services 1"> </dt>
				<dd>Slab Leaks</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svc2.jpg" alt="Services 2"> </dt>
				<dd>Plumbing Fixtures <br> and Repair</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svc3.jpg" alt="Services 3"> </dt>
				<dd>Water Heater - Repair <br> and Installation</dd>
			</dl>
		</div>
		<div class="container2">
			<dl>
				<dt> <img src="public/images/content/svc4.jpg" alt="Services 3"> </dt>
				<dd>Water Leaks</dd>
			</dl>
			<dl>
				<dt> <img src="public/images/content/svc5.jpg" alt="Services 4"> </dt>
				<dd>Sewer Repair and <br> Replacement</dd>
			</dl>
		</div>
		<div class="abtPanel">
			<h1>About Us</h1>
			<p>At J & T Plumbing Licensed and Insured, we offer a wide array of 24 hr plumbing services to make sure your fixtures are all working properly.</p>
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<div class="container">
			<p>Although we were profesionally established in 2017, we bring 25 years of experience, with good ratings and better prices. We work with you as if you were our family. We are always punctual, respectful, affordable, and cooperative. </p>
			<p>For everything, from gas to water line replacements and more, we have the expertise to give you what you are looking for. We can supply you with reliable tankless water heaters if your water heater is not working as it should. </p>
			<a href="services#content" class="btn">read more</a>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<div class="header">
			<h1>OUR GALLERY</h1>
		</div>
		<div class="container">
			<div class="gallLeft col-5 fl">
				<img src="public/images/content/gallery1.jpg" alt="Gallery 1">
				<a href="gallery#content" class="btn">view more</a>
			</div>
			<div class="gallRight col-7 fl">
				<img src="public/images/content/gallery2.jpg" alt="Gallery 2">
				<img src="public/images/content/gallery3.jpg" alt="Gallery 3">
				<img src="public/images/content/gallery4.jpg" alt="Gallery 4">
				<img src="public/images/content/gallery5.jpg" alt="Gallery 5">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<h2>CLIENT</h2>
		<h1>TESTIMONIALS</h1>
		<p>This company is quick to respond to service and got job done in great time!! Really good at communication and reliability to get work done! Also warranty his work! Great working with you jimmy!!!</p>
		<p>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</p>
		<img src="public/images/content/testImg.png" alt="profiles">
		<div class="discount">
			<h1>10% Discounts on Senior Citizens, Military and Teachers</h1>
			<p>PROFESIONALLY ESTABLISHED IN 2017, WE BRING 25 YEARS OF EXPERIENCE</p>
		</div>
	</div>
</div>
